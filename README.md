# Atilla's website

## Getting Started

* To install packages, run `npm install`

* Start the server: `npx punch s`

* Open your browser and visit: http://localhost:9009

## Build the static website

* Generate static files: `npx punch g`

The static site is in the **output** folder
